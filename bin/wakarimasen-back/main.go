package main

import (
	"fmt"
	"time"

	"bitbucket.org/drannoc/wakarimasen-back/pkg/mongo"
	"bitbucket.org/drannoc/wakarimasen-back/pkg/wakarimasen"
	"github.com/monkeydioude/moon"
)

const (
	mongoADDR = "mongodb://localhost:27017"
	connTO    = 3 * time.Second
)

func main() {
	err := mongo.Connect(mongoADDR, connTO)

	if err != nil {
		fmt.Println(err)
		return
	}
	server := moon.Moon()
	server.AddHeader("Access-Control-Allow-Origin", "*")
	server.AddHeader("Access-Control-Request-Method", "POST")
	server.AddHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	server.MakeRouter(
		moon.Get("/wakarimasen/vocab/list", wakarimasen.GetList),
		moon.Get("/wakarimasen/vocab/rand", wakarimasen.GetVocabRand),
		moon.Post("/wakarimasen/vocab/word", wakarimasen.PostVocabWord),
		moon.Options("/", wakarimasen.Options),
	)

	moon.ServerRun(":8000", server)
}
