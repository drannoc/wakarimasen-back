package wakarimasen

import (
	"bitbucket.org/drannoc/wakarimasen-back/pkg/mongo"
)

type Word struct {
	ID          string            `json:"id"`
	Type        string            `json:"type"`
	Label       string            `json:"word"`
	Hiragana    string            `json:"hiragana"`
	Traductions map[string]string `json:"traductions"`
}

func (w *Word) Spawn() mongo.Spawnable {
	return &Word{}
}
