package wakarimasen

import (
	"errors"
	"fmt"

	"bitbucket.org/drannoc/wakarimasen-back/pkg/mongo"
	"github.com/monkeydioude/moon"
)

func GetVocabRand(r *moon.Request) ([]byte, int, error) {
	return []byte("salut"), 200, nil
}

func GetList(r *moon.Request) ([]byte, int, error) {
	var w Word

	res, err := mongo.Database("wakarimasen").Collection("vocab").Find(mongo.None())

	if err != nil {
		return []byte(err.Error()), 500, err
	}

	b, err := res.JSONMarshal(&w)
	if err != nil {
		return []byte(err.Error()), 500, err
	}

	return b, 200, nil
}

func PostVocabWord(r *moon.Request) ([]byte, int, error) {
	err := r.HTTPRequest.ParseForm()

	if err != nil {
		return []byte("Could not parse Form"), 500, errors.New("Could not parse Form")
	}

	form := r.HTTPRequest.Form

	w := &Word{
		Label:       form.Get("word"),
		Type:        form.Get("type"),
		Hiragana:    form.Get("hiragana"),
		Traductions: make(map[string]string),
	}
	w.Traductions["eng"] = form.Get("traduction")
	res, err := mongo.Database("wakarimasen").Collection("vocab").InsertOne(w)

	if err != nil {
		return []byte("Could not insert in database"), 500, errors.New("Could not insert in database")
	}

	fmt.Println(res)
	return []byte("wesh"), 200, nil
}

func Options(r *moon.Request) ([]byte, int, error) {
	return []byte("ok"), 200, nil
}
